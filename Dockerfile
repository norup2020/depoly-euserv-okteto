FROM python:3.7-buster
WORKDIR /src
# 使用 Debian 清华源加速下载

COPY requirements.txt  requirements.txt
RUN pip install -r requirements.txt

ADD . /src

EXPOSE 8080